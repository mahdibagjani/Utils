package com.amin.utils;

import android.text.format.Time;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Calendar;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

/**
 * Created by Amin on 26/12/2017.
 */

public class GetPersianDate {

    public String fromTimeStomp(Long timeStomp) {

        long time = timeStomp * (long) 1000;
        Date date = new Date(time);
        SimpleDateFormat format = new SimpleDateFormat(" hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        Log.d("date", date.toString());
        return format.format(date);

    }

    public String fromTimeStomp(Long timeStomp, String Datepattern) {

        long time = timeStomp * (long) 1000;
        Date date = new Date(time);
        SimpleDateFormat format = new SimpleDateFormat(Datepattern);
        format.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        Log.d("date", date.toString());
        return format.format(date);

    }

    public Date getCurrentDate() {
        Date currentTime = Calendar.getInstance().getTime();
        return currentTime;
    }

    public Time getCurrentTime(){
        Time now = new Time();
        now.setToNow();
        return now;
    }

    public PersianDateFormat GetPersianDate(int year, int month, int day){
        PersianDate pdate = new PersianDate();
        pdate.toJalali(year,month,day);
        PersianDateFormat pdformater = new PersianDateFormat();
        pdformater.format(pdate);
        return pdformater;
    }

}
