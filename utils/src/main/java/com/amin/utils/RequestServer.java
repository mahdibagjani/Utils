package com.amin.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 *اتصال به سرور
 */

public class RequestServer {
    private String postResponse, getResponse, putResponse, baseUrl = null;
    private Context context;
    private HashMap<String, String> PutheaderMap = new HashMap<>();
    private HashMap<String, String> PostheaderMap = new HashMap<>();
    public RequestServer(Context context, String URL) {
        this.baseUrl =  URL;
        this.context = context;

    }

    public String getPutResponse() {
        return putResponse;
    }

    private void setPutResponse(String putResponse) {
        this.putResponse = putResponse;
    }

    public String getPostResponse() {
        return postResponse;
    }

    private void setPostResponse(String postResponse) {
        this.postResponse = postResponse;
    }

    public String getGetResponse() {
        return getResponse;
    }

    private void setGetResponse(String getResponse) {
        this.getResponse = getResponse;
    }

    public void Post(final HashMap<String, String> map) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, baseUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                setPostResponse(response);
                Log.e("From server ->Post", response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String body = null;
                final String statusCode = String.valueOf(error.networkResponse.statusCode);
                try {
                    body = new String(error.networkResponse.data, "UTF-8");

                    setPostResponse(body.toString());
                  //  Log.e("From server ->Post", body.toString());
                } catch (UnsupportedEncodingException e) {
                    // exception
                }


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {


                return PostheaderMap;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        int socketTimeout = 5000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);


    }

    public void Get() {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, baseUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                setGetResponse(response);
                Log.e("From server ->Get", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                String body = null;
                //get status code here
                final String statusCode = String.valueOf(error.networkResponse.statusCode);
                //get response body and parse with appropriate encoding

                    body = new String(error.networkResponse.data, "UTF-8");
                    setGetResponse(body.toString());
                    Log.e("From server ->Post", body.toString());
                    try {
                        JSONObject jsonObject =new JSONObject(body.toString());
                        if (jsonObject.getString("IsSuccessful")=="false")
                            Toast.makeText(context, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (NullPointerException e){

                } catch(UnsupportedEncodingException e) {
                    // exception
                }


            }
        });
        requestQueue.add(stringRequest);
    }

    public void Put(final HashMap<String, String> map) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, baseUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("From server ->Put", response);
                setPutResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = null;
                final String statusCode = String.valueOf(error.networkResponse.statusCode);
                try {
                    body = new String(error.networkResponse.data, "UTF-8");
                    setPutResponse(body.toString());
                     //  Log.e("From server ->Post", body.toString());
                } catch (UnsupportedEncodingException e) {
                    // exception
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {


                return PutheaderMap;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        requestQueue.add(stringRequest);

    }

    public void setPutHeaders(HashMap<String,String>headers){
        this.PutheaderMap=headers;
    }

    public void setPostHeaders(HashMap<String,String>headers){
        this.PostheaderMap=headers;
    }
}
