package com.amin.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

/**
*public void onActivityResult(int requestCode, int resultCode, Intent data) {
*if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
*CropImage.ActivityResult result = CropImage.getActivityResult(data);
*if (resultCode == RESULT_OK) {
*Uri resultUri = result.getUri();
*} else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
*Exception error = result.getError();
*}
*}
}*/

public class Crop {


    public void CropWithPicker(Activity activity, int aspectRatioX, int aspectRatioY) {

// start picker to get image for cropping and then use the image in cropping activity
        CropImage.activity()
                .setAutoZoomEnabled(true)
                .setAspectRatio(aspectRatioX, aspectRatioY)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(activity);
    }

    public void CropWithUri(Activity activity, Uri uri, int aspectRatioX, int aspectRatioY){
        CropImage.activity(uri)
                .setAspectRatio(aspectRatioX,aspectRatioY)
                .start(activity);
    }



}
