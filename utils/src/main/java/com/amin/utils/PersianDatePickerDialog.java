package com.amin.utils;

import android.content.Context;
import android.graphics.Color;
import android.widget.Toast;

import java.util.Date;

import ir.hamsaa.persiandatepicker.Listener;
import ir.hamsaa.persiandatepicker.util.PersianCalendar;

/**
 * Created by Amin on 26/12/2017.
 */

public class PersianDatePickerDialog {
    private Context context;
    private String PositiveButtonText="ok";
    private String NegativeButtonText="Cancel";
    private String TodayButtonText="Today";
    private boolean TodayButtonVisible=false;

    private int MaxYear=1400;
    private int MinYear=1300;
    private int ActionTextColor=Color.RED;
    private PersianCalendar InitDate=new PersianCalendar();

    private int year;
    private int month;
    private int day;

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public void setTodayButtonText(String todayButtonText) {
        TodayButtonText = todayButtonText;
    }

    public void setTodayButtonVisible(boolean todayButtonVisible) {
        TodayButtonVisible = todayButtonVisible;
    }

    public void setMaxYear(int maxYear) {
        MaxYear = maxYear;
    }

    public void setMinYear(int minYear) {
        MinYear = minYear;
    }

    public void setActionTextColor(int actionTextColor) {
        ActionTextColor = actionTextColor;
    }
    public void setInitDate(Date date){
        this.InitDate.setPersianDate(date.getYear(), date.getMonth(), date.getDay());
    }

    public PersianDatePickerDialog(Context context){
        this.context=context;
        ir.hamsaa.persiandatepicker.PersianDatePickerDialog picker = new ir.hamsaa.persiandatepicker.PersianDatePickerDialog(context)
                .setPositiveButtonString(PositiveButtonText)
                .setNegativeButton(NegativeButtonText)
                .setTodayButton(TodayButtonText)
                .setTodayButtonVisible(TodayButtonVisible)
                .setInitDate(InitDate)
                .setMaxYear(MaxYear)
                .setMinYear(MinYear)
                .setActionTextColor(ActionTextColor)

                .setListener(new Listener() {
                    @Override
                    public void onDateSelected(PersianCalendar persianCalendar) {

                        year = persianCalendar.getPersianYear();
                        month = persianCalendar.getPersianMonth();
                        day = persianCalendar.getPersianDay();

                    }

                    @Override
                    public void onDisimised() {

                    }
                });

        picker.show();
    }

}
