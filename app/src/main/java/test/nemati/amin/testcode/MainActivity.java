package test.nemati.amin.testcode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.amin.utils.Crop;
import com.amin.utils.GetPersianDate;
import com.amin.utils.Location;
import com.amin.utils.PersianDatePickerDialog;
import com.amin.utils.RequestServer;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Crop crop=new Crop();
        crop.CropWithPicker(MainActivity.this, 1, 1);

        Location location=new Location();
        location.getLocation(this);



    }

}
